import React, { Component } from 'react';
import { Button, Navbar, Nav, NavDropdown, Form, FormControl, ButtonToolbar, Modal } from 'react-bootstrap';
import logo from './Group (1).png';
import './navbar.css';
import Signin from './Components/Signin'
import Signup from './Components/Signup'


export default class NavBar extends Component {
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.logout= this.logout.bind(this)

    this.state = {
      show: false,
    };
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    
    localStorage.removeItem('user');
    this.setState({isAuthenticated: false, token: '', username: null})
    
    window.location.reload()
}

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
  componentDidMount(){
    
// $(document).ready(function(){
//   $('.tab a').on('click', function (e) {
//   e.preventDefault();
   
//   $(this).parent().addClass('active');
//   $(this).parent().siblings().removeClass('active');
   
//   var href = $(this).attr('href');
//   $('.forms > form').hide();
//   $(href).fadeIn(500);
// });
// });
//   }
  // handleClick() {
  //   if (this.state.isToggleOn){ 
      
  //     document.getElementById("myForm").style.display = "block";
  //   }
  //   else{
  //     document.getElementById("myForm").style.display = "none";
  //   }

  //   this.setState(state => ({
  //     isToggleOn: !state.isToggleOn
  //   }));
  }
    render() {
      var user= JSON.parse(localStorage.getItem("user"))
      return (
        <div className="Navbar"  >
        <Navbar  expand="lg" variant="dark" style={{backgroundColor : "transparent" }}>
        <Navbar.Brand href="#home"><img src={logo} className="App-logo" alt="logo" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />  
          <Navbar.Collapse id="basic-navbar-nav" >
            <Nav className="ml-auto">
              <Nav.Link href="#section1">Course</Nav.Link>
              <Nav.Link href="#section2">Our Event</Nav.Link>
            
              <Nav.Link href="#section2">About</Nav.Link>
              {!localStorage.getItem("token") ? (
                <div>
                  <Signin/>

              </div>
                ) :
                (
                  <div>
                    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Hello, {user.username}</a>
                    <ul class="dropdown-menu form-wrapper" style={{"width":"10px", "margin-right":'20px',"margin-top":'-20px'}}>					
                    <a href="/profile" style={{"textDecoration":"none", "color":"black"}}>My Profile</a>
          <li><a onClick={this.logout} style={{"textDecoration":"none", "color":"black"}}>Logout</a></li>
          
          </ul>
                </div>
                )
              }
            
      </Nav>
            </Navbar.Collapse>
          
        </Navbar>

              
      </div>
      )
    }
}