import React, { Component } from 'react';
import ScrollableAnchor from 'react-scrollable-anchor'
import { Button, Navbar, Nav, NavDropdown, Form, FormControl } from 'react-bootstrap';
import logo from './Mentoria.png';
import './App.css';

export default class Section1 extends Component {
    render() {
      return (
        <ScrollableAnchor id={'section1'}>
           <section className='container'> 
             <div className="one">
             <center>
               <img src={logo} width='300px' />
               </center>
             </div>
             <div className="two">
              <h3>
                <strong>About us</strong>
              </h3>
               <p align="left">
               Mentoria is an educational startup that focused on combining personalized learning with youth culture. We provide an innovative and disruptive concept of learning that help people to develop their talents. With our useful digital platform and experienced mentors, you will achieve your dream in no time with us.
               </p>
               <Button variant="light" size="xxl" href='#section2' >
                 Learn more
               </Button>
             </div>
           </section> 
      </ScrollableAnchor>
      )
    }
}