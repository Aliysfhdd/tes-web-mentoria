import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import { Route, Link, BrowserRouter as Router, Switch  } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Profile from './Profile'
import Signup from './Components/Signup';

const routing = (
    <Router>
      <div>
        <Switch>
        <Route exact path="/" component={App} />
        <Route path="/Register" component={Signup}/>
        <Route path="/profile" component={Profile} /> 
        </Switch>
      </div>
    </Router>
  )

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
