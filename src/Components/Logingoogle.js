import React from 'react';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import $ from 'jquery'


class Logingoogle extends React.Component {
  constructor() {
    super();
    this.state = { 
        isAuthenticated: false, 
        user: localStorage.getItem("user"), 
        token: localStorage.getItem('token'),
     };
    }
componentDidMount() {
    if (this.state.token != null) {
        this.setState({isAuthenticated:true})
           
// $(document).ready(function(){
//   $('.tab a').on('click', function (e) {
//   e.preventDefault();
   
//   $(this).parent().addClass('active');
//   $(this).parent().siblings().removeClass('active');
   
//   var href = $(this).attr('href');
//   $('.forms > form').hide();
//   $(href).fadeIn(500);
// });
// });      
      }
    }
  


googleResponse = (response) => {
    axios.post("http://localhost:8000/api/auth/login/google/",{"access_token":response.Zi.access_token}, {
        crossOrigin:true
    } )
    .then(r => {
     localStorage.setItem('token', r.data.token)
     axios.get("http://localhost:8000/api/me/", {
       crossOrigin:true, headers:{"Authorization":"Token "+r.data.token}
     }).then(data =>{
      localStorage.setItem('user', JSON.stringify(data.data))
      this.setState({isAuthenticated: true, token: r.data.token, user: JSON.stringify(data.data)})
  window.location.reload()
     })
     
  })
  .catch(error =>{
      alert("Anda telah menggunakan email di website ini")
  })
};

render() {
    return (
        
                 <GoogleLogin
    clientId="5038191851-tph4hrvonnnflk31gkur0ikpdem7k7qe.apps.googleusercontent.com"
    buttonText="Login"
    scope= "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
    accessType="online"
    responseType='permission'
    onSuccess={this.googleResponse}
    onFailure={this.onFailure}
    render={renderProps => (
    <button 
    className="btn btn-info pull-right" onClick={renderProps.onClick} disabled={renderProps.disabled}><i class="fa fa-google"/>Google</button>
    )}
    />
         
    );
}

} 

export default Logingoogle;
