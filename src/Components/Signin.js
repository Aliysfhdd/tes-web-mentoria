import React, { Component } from 'react';
import './signin.css'
import Logingoogle from './Logingoogle'
import axios from 'axios'



export default class Signin extends Component {
  constructor () {
    super();
    this.state = {
      email: '',
      password:'',
      isAuthenticated: false, 
      user: localStorage.getItem("user"), 
      token: localStorage.getItem('token'),
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit= this.handleSubmit.bind(this);
  }

  handleChange (event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  
  componentDidMount() {
    if (this.state.token != null) {
        this.setState({isAuthenticated:true})
    }
  }

  handleSubmit (event){
    event.preventDefault();
    const submit = {
      'email': this.state.email,
      'password'  : this.state.password
    };
    alert(JSON.stringify(submit));
    
    axios.post(`http://localhost:8000/api/auth/login/`,submit, 
    { 
      crossdomain: true,
    }
    )  
      .then(res => {
        
        localStorage.setItem("token", res.data.token)
        axios.get("http://localhost:8000/api/me/", {
       crossOrigin:true, headers:{"Authorization":"Token "+res.data.token}
     })
        .then(data =>{
          alert(data.data.username)
          localStorage.setItem('user', JSON.stringify(data.data))
          this.setState({isAuthenticated: true, token: res.data.token, user: JSON.stringify(data.data)})
          window.location.reload()
        })
        .catch(e=>{
       alert(e)
     })
     alert(JSON.stringify(res))
      })
    .catch(e =>{
        alert(e)
        alert("Invalid Email or Password")
      })
    }


    render() {
      return (
        <li class="nav-item">
				<a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Login</a>
				<ul class="dropdown-menu form-wrapper">					
					<li>
						<form onSubmit={this.handleSubmit} className="formSignin">
							<p class="hint-text">Login with your account </p>
							
							<div class="form-group">
								<input type="email" onChange={this.handleChange} name="email" class="form-control" placeholder="Email" required="required"></input>
							</div>
							<div class="form-group">
								<input type="password" onChange={this.handleChange} name="password" class="form-control" placeholder="Password" required="required"></input>
							</div>
							<input type="submit" class="btn btn-primary btn-block" value="Login"></input>
							<div class="form-footer">
								<a href="#">Forgot Your password?</a>
							</div>

							<div class="or-seperator"><b>or</b></div>
              <div class="form-group social-btn clearfix">
								<a href="#" class="btn btn-primary pull-left"><i class="fa fa-linkedin"></i> Linkedin</a>
                <Logingoogle/>
							</div>
              <div class="form-footer">
								Don't have account ?<a href="/Register"> Register</a>
							</div>
            </form>
					</li>
				</ul>
			</li>
      )
    }
  }