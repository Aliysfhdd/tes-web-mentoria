import React, { Component } from 'react';
import './signup.css'
import axios from 'axios'
import NavBar from '../NavBar';



export default class Signup extends Component {
  constructor () {
    super();
    this.state = {
      email: '',
      username:'',
      password1:'',
      password2:'',
      isAuthenticated: false, 
      user: localStorage.getItem("user"), 
      token: localStorage.getItem('token'),
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit= this.handleSubmit.bind(this);
  }

  handleChange (event) {
    this.setState({ [event.target.name]: event.target.value });
    
  }
  
  componentDidMount() {
    if (this.state.token != null) {
        this.setState({isAuthenticated:true})
    }
  }

  handleSubmit (event){
    event.preventDefault();
    if (this.state.password1 != this.state.password2){
      alert("Password yang anda masukan berbeda")
    }
    else if(this.state.username.length <8){
        alert("Username minimal 8 karakter")
    }
    else if(this.state.password1.length <8){
      alert("Password minimal 8 karakter")
    }
    else{
    const submit = {
      'username': this.state.username,
      'email': this.state.email,
      'password1'  : this.state.password1,
      'password2': this.state.password2
    };
    alert(JSON.stringify(submit));
    
    axios.post(`http://localhost:8000/api/registration/`,submit, 
    { 
      crossDomain: true,
    }
    )  
      .then(res => {
        
     alert("Akun anda telah berhasil terdaftar")
     window.location.href="/"
      })
    .catch(e =>{
        alert(e)
        alert("Email telah digunakan")
      })
    }
  }



    render() {
      return (
      	<div id="signup">
          <NavBar/>
         <div class="bg">
            <div class="signup form-wrapper">					
                <form onSubmit={this.handleSubmit}>
                  <p class="hint-text">Create your account!</p>
                  <div class="form-group">
                    <input type="text" class="form-control" name='username' onChange={this.handleChange} placeholder="Username" required="required"></input>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" name='email' onChange={this.handleChange} placeholder="Email" required="required"></input>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name='password1' onChange={this.handleChange} placeholder="Password" required="required"></input>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name='password2' onChange={this.handleChange} placeholder="Confirm Password" required="required"></input>
                  </div>
                  <div class="form-group">
                    <label class="checkbox-inline"><input type="checkbox" required="required"></input> I accept the <a href="#">Terms &amp; Conditions</a></label>
                  </div>
                  <input type="submit" class="btn btn-primary btn-block" value="Sign up"></input>
                </form>
            </div>
            </div>
            </div>
				
			)
    }
  }