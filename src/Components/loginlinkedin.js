import React from 'react';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import LinkedIn, { LinkedInPopUp } from 'react-linkedin-login-oauth2';
import $ from 'jquery'


class Signup extends React.Component {
    
  constructor() {
    super();
    this.state = { 
        isAuthenticated: false, 
        user: localStorage.getItem("user"), 
        token: localStorage.getItem('token'),
        code: '',
        errorMessage: '',
     };
}
componentDidMount() {
    if (this.state.token != null) {
        this.setState({isAuthenticated:true})
        console.log(this.state.token)
        axios.get('http://localhost:8000/api/users/'+ localStorage.getItem("user") +"/", {
          crossDomain:true, 'headers':{
            "Authorization":"Bearer "+ this.state.token
          }
        }
        )
        .then(res =>{
          alert(JSON.stringify(res.data))
        })
        .catch(r =>{
            console.log(r)
        }

            )
  
        
      }
    }
  

logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.setState({isAuthenticated: false, token: '', user: null})
};

handleSuccess = (data) => {
    this.setState({
      code: data.code,
      errorMessage: '',
    });
  }

  handleFailure = (error) => {
    this.setState({
      code: '',
      errorMessage: error.errorMessage,
    });
  }


render() {
    return (
        <div className="Linkedin">
            <LinkedIn
            state='foobar'
            scope="r_liteprofile r_emailaddress w_member_social"
          clientId="816ln4oq485p9v"
          onFailure={this.handleFailure}
          onSuccess={this.handleSuccess}
          redirectUri='http://localhost:8000/api/auth/login/linkedin_oauth2/callback/'
          >
        </LinkedIn>
        {!this.state.code && <div>No code</div>}
        {this.state.code && <div>Code: {this.state.code}</div>}
        {this.state.errorMessage && <div>{this.state.errorMessage}</div>}
        <div id= "my-signIn"></div>
        </div>
    );
}

} 

export default Signup;
