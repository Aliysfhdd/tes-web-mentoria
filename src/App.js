import React, { Component } from 'react';
import { Button, Navbar } from 'react-bootstrap';
import NavBar from './NavBar';
import Section2 from './Section2';
import './App.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

import Section1 from './Section1';
/**
 * @jest-environment jsdom
 */

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <div className="body">
        <NavBar/>
          <header className="App-home">
            <p>
              We Educate to Empower
          </p>
          <Link to="/signup">
            <Button variant="dark" size="xxl" href='#section1' className="button1" >
              EXPLORE
            </Button>
          </Link>
          </header>
        </div>
        <div className='section1'> 
          <Section1/>
        </div>
        <div className="section2">
          <Section2/>
          </div>
      </div>
    );
  }
} 

export default App;
