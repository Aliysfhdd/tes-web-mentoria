import React from 'react'
import ScrollableAnchor from 'react-scrollable-anchor'
import axios from "axios";

function parseJwt (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
};

class Section2 extends React.Component {
  constructor () {
    super();
    this.state = {
      username: '',
      password:'',
      persons:[],
      uid:'',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit= this.handleSubmit.bind(this);
  }
  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
      })
  }
  
  handleChange (event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  

  handleSubmit (event){
    event.preventDefault();
    const submit = {
      'username': this.state.username,
      'password'  : this.state.password
    };
    alert(JSON.stringify(submit));
    
    axios.post(`http://localhost:8000/api/token/`,submit, 
    { 
      crossDomain: true,
    }
    )  
      .then(res => {
        var uid= JSON.stringify(parseJwt(res.data.access).user_id);
        this.setState({uid})
        var access=res.data.access
        alert(JSON.stringify(res.data.access))
        axios.get('http://localhost:8000/api/users/'+ uid +"/", {
          crossDomain:true, 'headers':{
            "Authorization":"Bearer "+ access
          }
        }
        )
        .then(res =>{
          alert(JSON.stringify(res.data))
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response.headers);
          } 
          else if (error.request) {
              console.log(error.request);
          } 
          else {
            console.log(error.message);
          }
          console.log(error.config);
          alert(error)
        });

      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.headers);
        } 
        else if (error.request) {
            console.log(error.request);
        } 
        else {
          console.log(error.message);
        }
        console.log(error.config);
        alert(error)
      });
  }
  render() {
    return (
        <ScrollableAnchor id={'section2'}>
        <div>
          {this.state.uid}
          <center>
          <h1>List of User:</h1>
          { this.state.persons.map(person => 
        <li>{person.name}</li>)
        }
        <form onSubmit={this.handleSubmit}>
          <label>
            username
            <input type="text" name="username" onChange={this.handleChange} />
          </label>
          
          <label>
            password:
            <input type="text" name="password" onChange={this.handleChange} />
          </label>  
          
          <button type="submit">Login</button>
          
        </form>
          </center>
        </div>
        </ScrollableAnchor>
    )
  }
}
  
 
  
export default Section2